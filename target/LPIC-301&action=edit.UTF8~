__FORCETOC__
__NOTOCNUM__

==Introduction==

Capacity planning is the art and science of not running out of resources in the foreseeable future. It's often done informally, by measuring the resources that a program needs, commonly after having just run out of something.

If you make a table of how much CPU, memory and I/O bandwidth a program needs to do some unit of work, you can estimate how much it will need at some higher load in the future. Alternatively, you can use the measurements for sizing a new machine for the program, or for estimating how big a machine will be needed to consolidate your and other programs.

Informal spreadsheet estimates are often sufficient for simple sizing and future planning, but they do not have any correctness guarantees and they don't tell you:

* at what load the program will be overloaded, nor.
* how much the response time of the program will balloon under load. 

For that, you use one of the programs which solve the problem using queuing theory. There are commercial products which will do so on Linux, but at least one free queuing network solver exists, Perl::PDQ by Neil Gunther. These generate proper mathematical models, so you can predict the performance of the program under load, and calculate the drop-off in performance as the program becomes overloaded.

<br />

==Version Information==

These objectives are version 1.0.1.

<br />

==Addenda==

===''Addendum (Apr 1st, 2010)''===

* clarified C++ to mean C in development

<br />

==Translations of Objectives==

The following translations of the objectives are available on this wiki:

* [[LPIC-301|English]].
* [[LPIC-301(ES)|Spanish]].
* [[LPIC-301(FR)|French]]

<br />

==Objectives==

===''Topic 301: Concepts, Architecture and Design''===

====<span style="color:navy">301.1 LDAP Concepts and Architecture</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 3
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be familiar with LDAP and X.500 concepts 

|}

'''Key Knowledge Areas:'''

* LDAP and X.500 technical specification.
* Attribute definitions.
* Directory namespaces.
* Distinguished names.
* LDAP Data Interchange Format.
* Meta-directories.
* Changetype operations.

'''The following is a partial list of the used files, terms and utilities:'''

* LDIF
* Meta-directory
* changetype
* X.500
* /var/lib/ldap/*

<br />

====<span style="color:navy">301.2 Directory Design</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to design an implement an LDAP directory, while planning an appropriate Directory Information Tree to avoid redundancy. Candidates should have an understanding of the types of data which are appropriate for storage in an LDAP directory.

|}

'''Key Knowledge Areas:'''

* Define LDAP directory content.
* Organize directory.
* Planning appropriate Directory Information Trees.

'''The following is a partial list of the used files, terms and utilities:'''

* Class of Service
* Directory Information Tree
* Distinguished name
* Container

<br />

====<span style="color:navy">301.3 Schemas</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 3
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be familiar with schema concepts, and the base schema files included with an OpenLDAP installation.

|}

'''Key Knowledge Areas:'''

* LDAP schema concepts.
* Create and modify schemas.
* Attribute and object class syntax.

'''The following is a partial list of the used files, terms and utilities:'''

* Distributed schema
* Extended schema
* Object Identifiers
* /etc/ldap/schema/*
* Object class
* Attribute
* include directive 


<br />

===''Topic 302: Installation and Development''===

====<span style="color:navy">302.1 Compiling and Installing OpenLDAP</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 3
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to compile and install OpenLDAP from source and from packages.

|}

'''Key Knowledge Areas:'''

* Compile and configure OpenLDAP from source.
* Knowledge of OpenLDAP backend databases.
* Manage OpenLDAP daemons.
* Troubleshoot errors during installation .

'''The following is a partial list of the used files, terms and utilities:'''

* make
* gpg
* rpm
* dpkg
* bdb
* slapd
* slurpd
 
<br />

====<span style="color:navy">302.2 Developing for LDAP with Perl and C</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 1
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to write basic Perl scripts to interact with an LDAP directory.

|}

'''Key Knowledge Areas:'''

* Syntax of Perl's Net::LDAP module.
* Write Perl scripts to bind, search, and modify directories.

'''The following is a partial list of the used files, terms and utilities:'''

* Net::LDAP
* using Perl with Net::LDAP
* using C with libldap

<br />

===''Topic 303: Configuration''===

====<span style="color:navy">303.1 placeholder</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 0
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

This objective dropped due to JTA results.

|}

'''Key Knowledge Areas:'''

* N/A 

'''The following is a partial list of the used files, terms and utilities:'''

* N/A 

<br />

====<span style="color:navy">303.2 Access Control Lists in LDAP</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to plan and implement access control lists.

|}

'''Key Knowledge Areas:'''

* Plan LDAP access control lists.
* Grant and revoke LDAP access permissions.
* Access control syntax.

'''The following is a partial list of the used files, terms and utilities:'''

* ACL
* slapd.conf
* anonymous
* users
* self
* none
* auth
* compare
* search
* read
* write
 
<br />

====<span style="color:navy">303.3 LDAP Replication</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 5
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be familiar with the various replication strategies available with OpenLDAP.

|}

'''Key Knowledge Areas:'''

* Replication concepts.
* Configure OpenLDAP replication.
* Execute and manage slurpd.
* Analyze replication log files.
* Understand replica hubs.
* LDAP referrals.
* LDAP sync replication.

'''The following is a partial list of the used files, terms and utilities:'''

* slurpd
* slapd.conf
* master / slave server
* consumer
* replica hub
* one-shot mode
* referral
* syncrepl
* pull-based / push-based synchronization
* refreshOnly and refreshAndPersist
* replog

<br />

====<span style="color:navy">303.4 Securing the Directory</span>====
{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 4
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to configure encrypted access to the LDAP directory, and restrict access at the firewall level.

|}

'''Key Knowledge Areas:'''

* Securing the directory with SSL and TLS.
* Firewall considerations.
* Unauthenticated access methods.
* User / password authentication methods.
* Maintanence of SASL user DB.
* Client / server certificates.
 
'''The following is a partial list of the used files, terms and utilities:'''

* SSL / TLS
* Security Strength Factors (SSF)
* SASL
* proxy authorization
* StartTLS
* slapd.conf
* iptables

<br />

====<span style="color:navy">303.5 LDAP Server Performance Tuning</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be capable of measuring the performance of an LDAP server, and tuning configuration directives 

|}

'''Key Knowledge Areas:'''

* Measure LDAP performance.
* Tune software configuration to increase performance.
* Understand indexes.

'''The following is a partial list of the used files, terms and utilities:'''

* index
* slapd.conf
* DB_CONFIG

<br />

====<span style="color:navy">303.6 OpenLDAP Daemon Configuration</span>====
{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should have knowledge of the common slapd.conf configuration directives, and be familiar with the basic slapd command line options.

|}

'''Key Knowledge Areas:'''

* slapd.conf configuration directives.
* slapd.conf database definitions.
* slapd and its command line options.
* Analyze slapd log files.

'''The following is a partial list of the used files, terms and utilities:'''

* slapd.conf
* slapd
* /var/lib/ldap/*
* loglevel

<br />

===''Topic 304: Usage''===

====<span style="color:navy">304.1 Searching the Directory</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to use advanced options for search the LDAP directory.

|}

'''Key Knowledge Areas:'''

* Use OpenLDAP search tools with basic options.
* Use OpenLDAP search tools with advanced options.
* Optimize LDAP search queries.
* Knowledge of search filters and their syntax .

'''The following is a partial list of the used files, terms and utilities:'''

* ldapsearch
* index
* search filter syntax
* slapd.conf
 
<br />

====<span style="color:navy">304.2 LDAP Command Line Tools</span>====
{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 1
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be familiar with the OpenLDAP command line tools.

|}

'''Key Knowledge Areas:'''

* Use the ldap* tools to access and modify the directory.
* Use the slap* tools to access and modify the directory.

'''The following is a partial list of the used files, terms and utilities:'''

* ldap.conf
* ldapsearch
* ldapadd
* ldapmodify
* ldapdelete
* ldapmodrdn
* slapindex
* slapadd
* slapcat 

<br />
====<span style="color:navy">304.3 Whitepages</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 1
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to build and maintain a whitepages service.

|}
'''Key Knowledge Areas:'''

* Plan whitepages services.
* Configure whitepages services.
* Configure clients to retrieve data from whitepages services.

'''The following is a partial list of the used files, terms and utilities:'''

* whitepages
* Outlook 

<br />

===''Topic 305: Integration and Migration''===

====<span style="color:navy">305.1 LDAP Integration with PAM and NSS</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to configure PAM and NSS to retrieve information from an LDAP directory.

|}

'''Key Knowledge Areas:'''

* Configure PAM to use LDAP for authentication.
* Configure NSS to retrieve information from LDAP.
* Configure PAM modules in various Unix environments.

'''The following is a partial list of the used files, terms and utilities:'''

* PAM
* NSS
* /etc/pam.d/*
* /etc/nsswitch.conf

<br />

====<span style="color:navy">305.2 NIS to LDAP Migration</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 1
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to plan and implement a NIS migration strategy, including a NIS to LDAP gateway.

|}

'''Key Knowledge Areas:'''

* Analyze NIS structure prior to migration to LDAP.
* Analyze NIS structure prior to integration with LDAP.
* Automate NIS to LDAP migration.
* Create a NIS to LDAP gateway.

'''The following is a partial list of the used files, terms and utilities:'''

* NIS
* NIS to LDAP gateway
* slapd.conf
* /etc/yp/*

<br />

====<span style="color:navy">305.3 Integrating LDAP with Unix Services</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 1
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to integrate LDAP authentication with a number of common Unix services.

|}

'''Key Knowledge Areas:'''

* Integrate SSH with LDAP.
* Integrate FTP with LDAP.
* Integrate HTTP with LDAP.
* Integrate FreeRADIUS with LDAP.
* Integrate print services with LDAP.

'''The following is a partial list of the used files, terms and utilities:'''

* sshd.conf
* ftp
* httpd.conf
* radiusd.conf
* cupsd.conf
* ldap.conf

<br />

====<span style="color:navy">305.4 Integrating LDAP with Samba</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 1
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to integrate LDAP with Samba services.

|}

'''Key Knowledge Areas:'''

* Migrate from smbpasswd to LDAP.
* Understand OpenLDAP Samba schema.
* Understand LDAP as a Samba password backend.

'''The following is a partial list of the used files, terms and utilities:'''

* smb.conf
* smbpasswd
* samba3.schema
* slapd.conf 

<br />

====<span style="color:navy">305.5 Integrating LDAP with Active Directory</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to integrate LDAP with Active Directory Services.

|}

'''Key Knowledge Areas:'''

* Kerberos integration with LDAP.
* Cross platform authentication.
* Single sign-on concepts.
* Integration and compatibility limitations between OpenLDAP and Active Directory.

'''The following is a partial list of the used files, terms and utilities:'''

* Kerberos
* Active Directory
* single sign-on
* DNS 

<br />

====<span style="color:navy">305.6 Integrating LDAP with Email Services</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 1
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to integrate LDAP with email services.

|}

'''Key Knowledge Areas:'''

* Plan LDAP schema structure for email services.
* Create email attributes in LDAP.
* Integrate Postfix with LDAP.
* Integrate Sendmail with LDAP.
 
'''The following is a partial list of the used files, terms and utilities:'''

* Postfix
* Sendmail
* schema
* SASL
* POP
* IMAP 

<br />

===''Topic 306: Capacity Planning''===

====<span style="color:navy">306.1 Measure Resource Usage</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 4
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to measure hardware resource and network bandwidth usage.

|}

'''Key Knowledge Areas:'''

* Measure CPU usage.
* Measure memory usage.
* Measure disk I/O.
* Measure network I/O.
* Measure firewalling and routing throughput.
* Map client bandwidth usage.

'''The following is a partial list of the used files, terms and utilities:'''

* iostat
* vmstat
* pstree
* w
* lsof
* top
* uptime
* sar 

<br />

====<span style="color:navy">306.2 Troubleshoot Resource Problems</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 4
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to identify and troubleshoot resource problems.

|}
'''Key Knowledge Areas:'''

* Match / correlate system symptoms with likely problems.
* Identify bottlenecks in a system.

'''The following is a partial list of the used files, terms and utilities:'''

* swap
* processes blocked on I/O
* blocks in
* blocks out 

<br />

====<span style="color:navy">306.3 Analyze Demand</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to analyze capacity demands.

|}

'''Key Knowledge Areas:'''

* Identify capacity demands.
* Detail capacity needs of programs.
* Determine CPU / memory needs of programs.
* Assemble program needs into a complete analysis.

'''The following is a partial list of the used files, terms and utilities:'''

* PDQ
* CPU usage
* memory usage
* appropriate measurement time
* trend
* model
* what-if
* validate
* performance equation 

<br />

====<span style="color:navy">306.4 Predict Future Resource Needs</span>====

{|
| style="background:#dadada" | 

'''Weight'''

| style="background:#eaeaea" | 1
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Candidates should be able to monitor resource usage to predict future resource needs.

|}

'''Key Knowledge Areas:'''

* Predict capacity break point of a configuration.
* Observe growth rate of capacity usage.
* Graph the trend of capacity usage.

'''The following is a partial list of the used files, terms and utilities:'''

* diagnose
* predict growth
* average
* resource exhaustion

