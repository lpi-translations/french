__FORCETOC__
__NOTOCNUM__

==Introduction==

<br />

==Informations de version==

Ceci est la  version 1.0.0 des objectifs.

Ils proviennent en partie des objectifs des examens [[LPIC-3_301_Objectives|301]] et [[LPIC-3_302_Objectives|302]].  Vous trouverez également le [[LPIC2AndLPIC3SummaryVersion3To4|résumé et les informations détaillées]] concernant les modifications entre ces objectifs et les versions précédentes.

<br />

==Mises à jour==

===''Mise à jour au 1er octobre 2013''===

* sortie de la version 1.0.0

<br />

==Traductions des objectifs==

Les traductions suivantes sont disponibles sur ce wiki :

* [[LPIC-3_300_Objectives_V1|Anglais]]
* [[LPIC-3_300_Objectives_V1(FR)|Français]]
* [[LPIC-3_300_Objectives_V1(ES)|Espagnol]]

<br />

==Objectifs==

===''Sujet 390: Configuration d'OpenLDAP''===

====<span style="color:navy">390.1 Réplication avec OpenLDAP (valeur : 3)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 3
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent bien connaître les différentes stratégies de réplication serveur disponibles avec OpenLDAP.

|}

'''Domaines de connaissance les plus importants :'''

* Concepts autour de la réplication.
* Configuration de la réplication avec OpenLDAP.
* Analyse des journaux de réplication.
* Compréhension des concentrateurs de réplication (replica hub).
* Referrals LDAP.
* LDAP sync replication.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* serveur maître / esclave
* réplication multi-maîtres
* consommateur (consumer)
* concentrateur de réplication (replica hub)
* mode one-shot
* referral
* syncrepl
* synchronisation poussée ou tirée
* refreshOnly et refreshAndPersist
* replog

<br />

====<span style="color:navy">390.2 Securité de l'annuaire (valeur : 3)</span>====
{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 3
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure de mettre en place un accès chiffré à l'annuaire LDAP et de restreindre les accès au niveau du pare-feu.

|}

'''Domaines de connaissance les plus importants :'''

* Sécurité de l'annuaire avec SSL et TLS.
* Considérations sur la protection par pare-feu.
* Méthodes d'accès sans authentification.
* Méthodes d'authentification à partir d'un compte utilisateur et d'un mot de passe.
* Gestion d'une base de données utilisateur SASL.
* Certificats client / serveur.
 
'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* SSL / TLS
* Security Strength Factors (SSF)
* SASL
* délégation d'autorisation (proxy authorization)
* StartTLS
* iptables

<br />

====<span style="color:navy">390.3 Paramétrage des performances du serveur OpenLDAP (valeur : 2)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure d'évaluer les performances d'un serveur LDAP et de le paramétrer.

|}

'''Domaines de connaissance les plus importants :'''

* Mesure des performances du serveur OpenLDAP.
* Règlage de la configuration du serveur pour améliorer les performances.
* Compréhension des index.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* index
* DB_CONFIG

<br />

===''Sujet 391 : OpenLDAP en tant que base d'authentification''===

====<span style="color:navy">391.1 Intégration de LDAP avec PAM et NSS (valeur : 2)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure de configurer PAM et NSS pour qu'ils récupèrent les informations à partir d'un annuaire LDAP.

|}

'''Domaines de connaissance les plus importants :'''

* Configuration de PAM pour une authentification LDAP.
* Configuration de NSS pour récupérer les informations à partir de LDAP.
* Configuration des modules PAM dans les différents environnements Unix.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* PAM
* NSS
* /etc/pam.d/*
* /etc/nsswitch.conf

<br />

====<span style="color:navy">391.2 Intégration de LDAP avec Active Directory et Kerberos (valeur : 2)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure d'intégrer LDAP avec les services Active Directory.

|}

'''Domaines de connaissance les plus importants :'''

* Intégration de Kerberos avec LDAP.
* Authentification multi-plateformes.
* Concepts de l'authentification unique (Single Sign-On).
* Intégration et limites de compatibilité entre OpenLDAP et Active Directory.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* Kerberos
* Active Directory
* Authentification unique (Single Sign-On)
* DNS 

<br />

===''Sujet 392 : Fondamentaux sur Samba''===

====<span style="color:navy">392.1 Architecture et concepts de Samba (valeur : 2)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent comprendre les concepts essentiels de Samba.  De plus, les candidats doivent connaître les différences principales entre Samba3 et Samba4.

|}

'''Domaines de connaissance les plus importants :'''

* Compréhension du rôle des services et des composants de Samba.
* Compréhension des problèmes clés liés aux réseaux hétérogènes.
* Connaissance des ports TCP et UDP clés utilisés par SMB/CIFS.
* Connaissance des différences entre Samba3 et Samba4.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* /etc/services.
* Services Samba : smbd, nmbd, samba, winbindd.

<br />

====<span style="color:navy">392.2 Configuration de Samba (valeur : 4)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 4
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure de configurer les services Samba pour une large variété de besoins.

|}

'''Domaines de connaissance les plus importants :'''

* Connaissance de la structure du fichier de configuration du serveur Samba.
* Connaissance des variables et des paramètres de configuration de Samba.
* Résolution de problèmes de configuration de Samba.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* smb.conf.
* Paramètres de smb.conf.
* Variables de smb.conf.
* testparm.
* secrets.tdb.

<br />

====<span style="color:navy">392.3 Maintenance courante de Samba (valeur : 2)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''


| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent connaître les différents outils et services faisant partie de l'installation de Samba.

|}

'''Domaines de connaissance les plus importants :'''

* Suivi et interaction avec les services Samba en cours d'exécution.
* Sauvegardes courantes de la configuration et des données d'état de Samba.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* smbcontrol.
* smbstatus.
* tdbbackup.

<br />

====<span style="color:navy">392.4 Résolution de problèmes avec Samba (valeur : 2)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent comprendre la structure des fichiers triviaux de base de données et savoir comment résoudre les problèmes liés.

|}

'''Domaines de connaissance les plus importants :'''

* Configuration de la journalisation de Samba.
* Sauvegarde des fichiers TDB.
* Restauration des fichiers TDB.
* Identification de la corruption  d'un fichier TDB.
* Modifier ou afficher le contenu d'un fichier TDB.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* /var/log/samba/*
* niveau de journalisation (log level).
* niveau de deboguage (debuglevel).
* smbpasswd.
* pdbedit.
* secrets.tdb.
* tdbbackup.
* tdbdump.
* tdbrestore.
* tdbtool.

<br />

====<span style="color:navy">392.5 Internationalisation (valeur : 1)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 1
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure de travailler avec les jeux de caractères nationaux et les pages de code.

|}

'''Domaines de connaissance les plus importants :'''

* Compréhension des jeux de caractères nationaux et des pages de code.
* Compréhension de la différence entre Windows et Linux/Unix dans les conventions de nommage utilisées pour les noms de partages, fichiers et répertoires dans un environnement autre que l'anglais.
* Compréhension de la différence entre Windows et Linux/Unix dans les conventions de nommage utilisées pour les noms d'utilisateurs et de groupes dans un environnement autre que l'anglais.
* Compréhension de la différence entre Windows et Linux/Unix dans les conventions de nommage utilisées pour les noms de machines dans un environnement autre que l'anglais.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* internationalisation.
* jeux de caractères.
* pages de code.
* smb.conf.
* Jeux de caractères DOS, utilisés pour l'affichage et Unix.

<br />

===''Sujet 393 : Configuration des partages Samba''===

====<span style="color:navy">393.1 Partages de fichiers (valeur : 4)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 4
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure de mettre en place et de configurer des partages de fichiers dans un environnement hétérogène.

|}

'''Domaines de connaissance les plus importants :'''

* Mise en place et configuration de partages de fichiers.
* Élaboration de la migration d'un serveur de partage.
* Limitation de l'accès à IPC$.
* Création de scripts pour la gestion des partages des utilisateurs et des groupes.
* Paramètres de configuration des accès aux partages Samba.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* smb.conf.
* [homes].
* smbcquotas.
* smbsh.
* browseable, writeable, valid users, write list, read list, read only et guest ok.
* IPC$.
* mount, smbmount.
<br />

====<span style="color:navy">393.2 Permissions sur le système de fichiers Linux et les partages (valeur : 3)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 3
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent comprendre la mise en œuvre des permissions sur un système de fichiers Linux dans un environnement hétérogène.

|}

'''Domaines de connaissance les plus importants :'''

* Connaissance de la gestion des permissions sur les fichiers et les répertoires.
* Compréhension des interactions entre Samba et les permissions Linux ou les LCA (listes de contrôle d'accès - ACL).
* Utilisation de Samba VFS pour stocker les LCA (ACL) Windows.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* smb.conf.
* chmod, chown.
* create mask, directory mask, force create mode, force directory mode.
* smbcacls.
* getfacl, setfacl.
* vfs_acl_xattr, vfs_acl_tdb et vfs objects.
<br />

====<span style="color:navy">393.3 Services d'impression (valeur : 2)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure de mettre en place et de gérer des partages d'impression dans un environnement hétérogène.

|}

'''Domaines de connaissance les plus importants :'''

* Mise en place et configuration des partages d'impression.
* Configuration de l'intégration entre Samba et CUPS.
* Gestion des pilotes d'impression Windows et configuration du téléchargement des pilotes d'impression.
* Configuration de [print$].
* Compréhension des problèmes de sécurité liés aux partages d'impression.
* Mise en place des pilotes d'impression pour l'installation des pilotes via l'assistant d'ajout de pilote d'impression sous Windows.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* smb.conf.
* [print$].
* CUPS.
* cupsd.conf.
* /var/spool/samba.
* smbspool.
* rpcclient.
* net.
<br />

===''Sujet 394 : Gestion des utilisateurs et des groupes Samba''===

====<span style="color:navy">394.1 Gestion des comptes utilisateurs et des groupes (valeur : 4)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 4
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure de gérer les comptes utilsiateurs et les groupes dans un environement hétérogène.

|}

'''Domaines de connaissance les plus importants :'''

* Gestion des comptes utilisateurs et des groupes.
* Compréhension de la mise en correspondance des utilisateurs et groupes Unix / Windows (user and group mapping).
* Connaissance des outils de gestion des comptes utilisateurs.
* Utilisation de la commande smbpasswd.
* Imposition des propriétés sur les fichiers et répertoires.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* pdbedit.
* smb.conf.
* samba-tool user (et sous-commandes).
* samba-tool group (et sous-commandes).
* smbpasswd.
* /etc/passwd.
* /etc/group.
* force user, force group.
* idmap.
<br />

====<span style="color:navy">394.2 Authentification, autorisation et Winbind (valeur : 5)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 5
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent comprendre les différents mécanismes d'authentification et mettre en place les contrôle d'accès.  Les candidats doivent être en mesure d'installer et de mettre en place le service Winbind.

|}

'''Domaines de connaissance les plus importants :'''

* Mise en place d'une base de données de mots de passe locale.
* Synchronisation des mots de passe.
* Connaissance des différents moteurs (backend) passdb.
* Conversion entre les différents moteurs passdb Samba.
* Integration de Samba à LDAP.
* Configuration du service Winbind.
* Configuration de PAM et NSS.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* smb.conf.
* smbpasswd, tdbsam, ldapsam.
* passdb backend.
* libnss_winbind.
* libpam_winbind.
* libpam_smbpass.
* wbinfo.
* getent.
* SID et foreign SID.
* /etc/passwd.
* /etc/group.

<br />

===''Sujet 395 : Samba et domaines Windows''===

====<span style="color:navy">395.1 Samba en tant que contrôleur de domaine principal (PDC) ou contrôleur de domaine secondaire (BDC) (valeur : 3)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 3
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure de mettre en place et de gérer des contrôleurs de domaines principaux et secondaires.  Les candidats doivent être en mesure de gérer l'accès des clients Windows et Linux aux domaines de type NT.

|}

'''Domaines de connaissance les plus importants :'''

* Compréhension et configuration de l'appartenance au domaine et des relations de confiance.
* Mise en place et gestion d'un contrôleur de domaine principal avec Samba3 et Samba4.
* Mise en place et gestion d'un contrôleur de domaine secondaire avec Samba3 et Samba4.
* AJout d'ordinateurs à un domaine existant.
* Configuration des scripts de connexion.
* Configuration des profils itinérants.
* Configuration des politiques système.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* smb.conf.
* security mode.
* server role.
* domain logons.
* domain master.
* logon script.
* logon path.
* NTConfig.pol.
* net.
* profiles.
* add machine script.
* listes de contrôle d'accès (ACL) pour les profils.

<br />

====<span style="color:navy">395.2 Contrôleur de domaine compatible AD avec Samba4 (valeur : 3)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 3
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure de configurer Samba 4 pour faire office contrôleur de domaine compatible AD (Active Directory).

|}

'''Domaines de connaissance les plus importants :'''

* Configuration et test de Samba 4 comme contrôleur de domaine AD.
* Utilisation de smbclient pour vérifier le fonctionnement de l'active directory.
* Compréhension de l'intégration de Samba avec les services AD : DNS, Kerberos, NTP et LDAP.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* smb.conf.
* server role.
* samba-tool domain (et sous-commandes).
* samba.

<br />

====<span style="color:navy">395.3 Configuration de Samba en tant que serveur membre du domaine (valeur : 3)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 3
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure d'intégrer des serveurs Linux à un environnement utilisant Active Directory.

|}

'''Domaines de connaissance les plus importants :'''

* Enregistrement de Samba sur un domaine NT4 existant.
* Enregistrement de Samba sur un domaine AD existant.
* Capacité à obtenir un TGT (Ticker Granting Ticket) à partir d'un KDC (Key Distribution Center).

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* smb.conf.
* server role.
* server security.
* commande net.
* kinit, TGT et REALM.

<br />
<br />

===''Sujet 396 : Service de nom Samba''===



====<span style="color:navy">396.1 NetBIOS et WINS (valeur : 3)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 3
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être familiarisés avec les concepts concernant NetBIOS/WINS et comprendre l'exploration du réseau.

|}

'''Domaines de connaissance les plus importants :'''

* Compréhension des concepts liés à WINS.
* Compréhension des concepts liés à NetBIOS.
* Compréhension du rôle d'un serveur de navigation maître local.
* Compréhension du rôle d'un serveur de navigation maître pour le domaine.
* Compréhension du rôle de Samba en tant que serveur WINS.
* Compréhension de la résolution de noms de machines.
* Configuration de Samba en tant que serveur WINS.
* Configuration de la réplication WINS.
* Compréhension du parcours réseau NetBIOS et des elections de navigateur.
* Compréhension des types de noms NetBIOS.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* smb.conf.
* nmblookup.
* smbclient.
* name resolve order.
* lmhosts.
* wins support, wins server, wins proxy, dns proxy.
* domain master, os level, preferred master.

<br />

====<span style="color:navy">396.2 Résolution de nom Active Directory (valeur : 2)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être familiarisés avec le serveur DNS de Samba4.

|}

'''Domaines de connaissance les plus importants :'''

* Compréhension et gestion du DNS avec Samba4 en tant que contrôleur de domaine AD.
* Transfert DNS avec le serveur DNS interne de Samba4.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* samba-tool dns (et sous-commandes).
* smb.conf.
* dns forwarder.
* /etc/resolv.conf.
* dig, host.

<br />

===''Sujet 397 : Travail avec les clients Linux et Windows''===

====<span style="color:navy">397.1 Intégration CIFS (valeur : 3)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 3
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être à l'aise dans un environnement CIFS hétérogène.

|}

'''Domaines de connaissance les plus importants :'''

* Compréhension des concepts liés à SMB/CIFS.
* Accès et montage de partages CIFS distants à partir d'un poste Linux.
* Stockage sécurisé des informations d'authentification CIFS.
* Compréhension des fonctionnalités et avantages de CIFS.
* Compréhension des permissions d'accès et propriétés (utilisateur / groupe) des fichiers sur les partages CIFS distants.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* SMB/CIFS.
* mount, mount.cifs.
* smbclient.
* smbget.
* smbtar.
* smbtree.
* findsmb.
* smb.conf.
* smbcquotas.
* /etc/fstab.

<br />

====<span style="color:navy">397.2 Travail avec des clients Windows (valeur : 2)</span>====

{|
| style="background:#dadada" | 

'''Valeur'''

| style="background:#eaeaea" | 2
|-
| style="background:#dadada; padding-right:1em" | 

'''Description'''

| style="background:#eaeaea" | 

Les candidats doivent être en mesure d'interagir avec les clients WIndows distants, ainsi que de configurer les postes Windows pour accéder aux partages de fichiers et d'impression sur les serveurs Linux.

|}

'''Domaines de connaissance les plus importants :'''

* Connaissance des clients Windows.
* Consultation des listes d'exploration et clients SMB sur Windows.
* Accès aux partages de fichiers et d'impression depuis Windows.
* Utilisation de la commande smbclient.
* Utilisation de la commande Windows net.

'''Liste partielle de termes, fichiers et utilitaires utilisés pour cet objectif :'''

* commande Windows net.
* smbclient.
* Panneau de configuration.
* rdesktop.
* workgroup.

<br />